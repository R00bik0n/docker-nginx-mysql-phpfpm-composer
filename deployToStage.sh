#!/bin/bash
cd /home/rubikon/staging
rm -rf hospital
git clone git@bitbucket.org:R00bik0n/tmp.git hospital
cd hospital
sed -i 's/80/8888/' docker-compose.yml
sed -i 's/3306/33061/' docker-compose.yml
docker-compose down
docker-compose up --build -d
echo "Check staging server!"
