#!/bin/bash
cd /home/rubikon/production
rm -rf hospital
cp -r /home/rubikon/staging/hospital/ /home/rubikon/production/hospital
cd hospital
chmod -R 777 var/log/
sed -i 's/8888/80/' docker-compose.yml
sed -i 's/33061/3306/' docker-compose.yml
docker-compose down
docker-compose up --build -d
echo "Check production server!"
